$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\Universal SiLA Client.lnk" -TargetPath "$path\tools\usc.exe"
Install-ChocolateyPinnedTaskBarItem -TargetFilePath "$path\tools\usc.exe"