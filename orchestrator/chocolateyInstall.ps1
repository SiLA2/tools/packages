$path = Get-ChocolateyPath -PathType 'PackagePath'
$desktop = [Environment]::GetFolderPath("Desktop")
Install-ChocolateyShortcut -ShortcutFilePath "$desktop\SiLA Orchestrator.lnk" -TargetPath "$path\tools\sila-orchestrator.exe"
Install-ChocolateyPinnedTaskBarItem -TargetFilePath "$path\tools\sila-orchestrator.exe"