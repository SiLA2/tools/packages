# SiLA Installer Packages

This repository hosts nuspec files to automatically build packages of other repositories. It is meant as a temporary working directory 
for package build instructions until they have been integrated into the respective repositories directly.

## Create packages

In order to create a package,

- [Install Chocolatey](https://docs.chocolatey.org/en-us/choco/setup)
- Copy the binaries (the nuspec file tells you which files)
- Update the version in the nuspec file
- Call `choco pack nuspecfilename.nuspec`

This will create a *nupkg* package file. This can be uploaded either manually through the [ProGet](https://silainstaller.gwdguser.de/) user interface or automatically using an API key.